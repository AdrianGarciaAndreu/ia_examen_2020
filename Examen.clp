(deffacts datos
	(robot 0 platos_llevar 0 platos_recoger posicion 0)
	(recoger mesa 1 platos 1)
	(cocina mesa 1 platos 2)
	(cocina mesa 2 platos 1)
)

(defrule entregar_platos
	?r <- (robot ?c_platos_llevar platos_llevar ?c_platos_r platos_recoger posicion 0)
	(test (> ?c_platos_r 0))
	=>
	(assert (robot 0 platos_llevar 0 platos_recoger posicion 0))
	(retract ?r)
)

(defrule coger_platos
	?r <- (robot ?c_platos_ll platos_llevar ?c_platos_r platos_recoger posicion 0)
	?c <- (cocina mesa ?n_mesa platos ?n_platos)

	(test (< (+ (+ ?c_platos_ll ?c_platos_r ) ?n_platos) 5))
	=>
	(assert (robot (+ ?c_platos_ll ?n_platos) platos_llevar ?c_platos_r platos_recoger posicion 0))
	(retract ?r)
	(assert (cocina mesa ?n_mesa platos ?n_platos llevando))
	(retract ?c)

)

(defrule llevar_platos
	?r <- (robot ?c_platos_ll platos_llevar ?c_platos_r platos_recoger posicion ?p)
	?c <- (cocina mesa ?n_mesa platos ?n_platos llevando)

	=>
	(assert (robot (- ?c_platos_ll ?n_platos) platos_llevar ?c_platos_r platos_recoger posicion ?n_mesa))
(retract ?r)
(retract ?c)
)



(defrule recoger_platos
	?r <- (robot ?c_platos_ll platos_llevar ?c_platos_r platos_recoger posicion ?p)
	?c <- (recoger mesa ?n_mesa platos ?n_platos)
	
	(test (< (+ (+ ?c_platos_ll ?c_platos_r ) ?n_platos) 5))
	=>
(assert (robot ?c_platos_ll platos_llevar (+ ?c_platos_r ?n_platos) platos_recoger posicion ?n_mesa))
(retract ?r)
(retract ?c)

)

(defrule volver_cocina
	?r <- (robot ?c_platos_ll platos_llevar ?c_platos_r platos_recoger posicion ?p)
	(test (= ?c_platos_ll 0))
	(test (<> ?p 0))
	=>
	(assert (robot ?c_platos_ll platos_llevar ?c_platos_r platos_recoger posicion 0))
	(retract ?r)
)
